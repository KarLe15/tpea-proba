open Plplot;;
open Random;;
open Array;;

(** Changer la graine de l'aléa au début du programme**)
Random.self_init ()

(** Fonction pour tracer un histogramme 
    Exemple d'utilisation: 
    let () = histo data (abscisse_minimum) (abscisse_maximum) (nbre_bins) "myfile.svg" "#fr Étiquette des abscisses" "#fr Étiquette des ordonnées" "#fr Titre"

    Commande pour compiler le programme
    ocamlfind opt -package plplot -linkpkg -o histo fichier.ml


    Commande pour exécuter le programme
    ./histo

    Ensuite on peut afficher le svg dont on a donner le nom "myfile.svg"
 **)

let histo data xmin xmax bin fname lx ly title=
  (* Initialize plplot *)
  plsdev "svg";
  plsfnam fname;
  plinit ();

  (* Fill up data points *)
  plhist data xmin xmax bin [PL_HIST_DEFAULT]; 

  plcol0 1;
  plcol0 2;
  pllab lx ly title;

  plend ();
  ()

let print_data d =
  Printf.printf "[| ";
  iter (Printf.printf "%f ; ") d;
  Printf.printf " |]"; ()


(** Définit la variable aléatoire (le modèle) associée à l'expérience de Galton **)

(* une pièce équilibrée *)
let coin = Random.bool

let biaised_coin t = 
  let r = Random.float 1. in
  r < t 
;;

(* planche de Galton de hauteur n*)
let rec galton (n:int) =
  match n with
  | 0 -> 0.
  | _ -> if coin() then  galton (n-1) +. 0.5 else galton (n-1) -. 0.5

let rec fbirth (p:int) (t:float) = 
  match p with 
  | 0 -> 0.
  | _ -> if (biaised_coin t) then (fbirth (p-1) t ) +. 1. else (fbirth (p-1) t )

let esperance data = 
  let size = Array.length data in 
  let sum = Array.fold_left (fun acc x -> acc +. x) 0. data in 
  sum /. (float_of_int size)
;;
(** On échantillonne, et on trace **)
(* sampling creates an array of n samples following law d *)
let sampling  (n:int) (d: unit -> 'a) =
  let y = Array.make n () in
  Array.map d y;;

let data = sampling 1000 (function ()-> (galton 20))

let data_tp = sampling 1000 (fun () -> fbirth 20 0.5)

let xmin = 0.
let xmax = 20.

let () =
  histo data_tp xmin xmax 20 "fbirth.svg" "#frValeur de la variable aléatoire" "#frNombre d'occurences" "#frExemple de modélisation: FBirth" ;
  (* print_data data_tp;  *)
  print_float (esperance data_tp); ()


(* ocamlfind opt -package plplot -linkpkg -o fbirth tp.ml 
   ./galton
*)
