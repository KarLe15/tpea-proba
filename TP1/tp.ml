open Random
open Array

let printArray y = 
  let rec aux y =
    match y with 
    | x::xs -> print_int x; aux xs
    | [] -> ()
  in 
    aux y

(* sampling creates an array of n samples following law d *)
let sampling  (n:int) (d: unit -> 'a) =
  let y = Array.make n () in
  Array.map d y


(* rejection_sampling creates an array of n samples satisfying 
predicate p and the model d *)

let rejection_sampling (n:int) (d:unit -> 'a)  (p:'a -> bool) =
  let y = Array.make n () in
  let rec correct () =
    let value = d () in
    match p value with
      | true  -> value
      | false -> correct ()
  in 
    Array.map correct y

(* rejection_sampling 5 (fun () -> Random.int 50) (fun a -> a < 2) *)



(* Approximate mean value *)

let mean (m : unit -> 'a) (f : 'a -> float) =
  0. (* TO CHANGE *)


(* Coins *)
(** Fair *)
let coin =
  0.  (* TO CHANGE *)l

(** Biased *)

let flip (theta:float) =
  0.  (* TO CHANGE *)
  
(* Models *)
(** Flip n biased coin *)

let rec nflip (n:int) (theta:float) =
  let d = Array.make n true in
  d  (* TO CHANGE *)

(** Galton *)

let rec galton (n:int) =
  0  (* TO CHANGE *)


(** Biased Galton *)

let rec plinko (n:int) (theta:float) =
  0  (* TO CHANGE *)

(** Female births *)
  
let fBirth (theta: float) (b: int)=
  0  TO CHANGE


(* Simulate models, using sampling, and approximate mean value *)


(* Add conditions, using rejection sampling, and approximate mean value *)

(** more true than false **)

(** plinko with positive gain **)

(** biased more than twice **)
